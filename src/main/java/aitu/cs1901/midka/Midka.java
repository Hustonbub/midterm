package aitu.cs1901.midka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Midka {

	public static void main(String[] args) {
		SpringApplication.run(Midka.class, args);
	}

}
