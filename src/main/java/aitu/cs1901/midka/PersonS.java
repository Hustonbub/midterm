package aitu.cs1901.midka;

import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class PersonS {
    public final PersonR personRepository;

    public PersonS(PersonR personRepository) {
        this.personRepository = personRepository;
    }

    public List<Person> getAll(){
        return (List<Person>) personRepository.findAll();
    }

    public Person getById(Long id) {
        return personRepository.findById(id).orElse(null);
    }

    public Person create(Person person) {
        return personRepository.save(person);
    }

    public Person update(Person person) {
        return personRepository.save(person);
    }

    public void delete(Long id) {
        personRepository.deleteById(id);
    }
}