package aitu.cs1901.midka;

import org.springframework.data.repository.CrudRepository;

public interface PersonR extends CrudRepository<Person, Long>{
}